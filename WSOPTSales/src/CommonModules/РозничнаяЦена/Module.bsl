функция ЦенаТовара(АктуальнаяДата, ЭлементТовара) экспорт
	Отбор = новый Структура("Товары", ЭлементТовара);
	ЗначениеРесурса = РегистрыСведений.ЦеныНаТовары.Получить(АктуальнаяДата, Отбор);
	возврат ЗначениеРесурса.Цена;
конецфункции
